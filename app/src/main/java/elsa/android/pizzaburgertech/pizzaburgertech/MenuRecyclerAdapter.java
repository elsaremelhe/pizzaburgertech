package elsa.android.pizzaburgertech.pizzaburgertech;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MenuRecyclerAdapter extends RecyclerView.Adapter<MenuRecyclerAdapter.ViewHolder> implements Parcelable {

    private Context nContext;
    private List<MenuItem> nData;
    private ArrayList<String> list = new ArrayList<String>();
    private ArrayAdapter<String> arrayAdapter;
    private static final String SHARED_PREFS_NAME = "MY_SHARED_PREF";
    Dialog mDialog;
    float count = 0;


    static List<MenuCart> menuCart = new ArrayList<>();


    public MenuRecyclerAdapter(Context nContext, List<MenuItem> nData) {
        this.nContext = nContext;
        this.nData = nData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater xInflater = LayoutInflater.from(nContext);
        view = xInflater.inflate(R.layout.menu_item,viewGroup,false);
        ViewHolder vHolder = new ViewHolder(view);


        mDialog = new Dialog(nContext);
        mDialog.setContentView(R.layout.popup_reviews);



        vHolder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked on buy " + String.valueOf(vHolder.getAdapterPosition()) + " !");
                menuCart.add(new MenuCart(nData.get(vHolder.getAdapterPosition()).getMenuPrice(), String.valueOf(nData.get(vHolder.getAdapterPosition()).getMenuName()), 1));
                System.out.println("Size of the array: " + menuCart.size());
                Toast.makeText(nContext, "Product added to cart", Toast.LENGTH_SHORT).show();
            }
        });

        vHolder.menuitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView dialog_name = mDialog.findViewById(R.id.itemName);
                TextView dialog_price = mDialog.findViewById(R.id.itemPrice);
                TextView dialog_avrating = mDialog.findViewById(R.id.itemAvgRate);
                ImageView dialog_image = mDialog.findViewById(R.id.itemImage);
                ListView dialog_list = mDialog.findViewById(R.id.reviewList);
                CardView dialog_button = mDialog.findViewById(R.id.reviewSubmit);
                RatingBar dialog_rating = mDialog.findViewById(R.id.ratingBar);

                dialog_name.setText(nData.get(vHolder.getAdapterPosition()).getMenuName());
                dialog_price.setText(String.valueOf(nData.get(vHolder.getAdapterPosition()).getMenuPrice()) + "$");
                dialog_image.setImageResource(nData.get(vHolder.getAdapterPosition()).getMenuImage());

                if(list.size()==0) {
                    dialog_avrating.setText("Rating: " + 0.0);
                }

                list = getArray();
                arrayAdapter = new ArrayAdapter<String>(nContext, R.layout.review_list, R.id.reviewLine,list);


                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText edit = mDialog.findViewById(R.id.itemReview);
                        list.add( "Rating: " + dialog_rating.getRating() + "                                        " + edit.getText().toString());


                        if(edit.getText() != null){
                            count+= dialog_rating.getRating();
                            edit.setText("");
                            float avg = (count)/list.size();
                            Toast.makeText(nContext, "Review Submited",Toast.LENGTH_LONG).show();
                            dialog_avrating.setText(String.format("Rating: %.1f", avg));
                        }

                        arrayAdapter.notifyDataSetChanged();
                    }
                });
                dialog_list.setAdapter(arrayAdapter);
                Toast.makeText(nContext,"Clicked on Menu " + String.valueOf(vHolder.getAdapterPosition()),Toast.LENGTH_SHORT).show();

                mDialog.show();
            }
        });

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.menuname.setText(nData.get(i).getMenuName());
        viewHolder.menuprice.setText(String.valueOf(nData.get(i).getMenuPrice()));
        viewHolder.menuimage.setImageResource(nData.get(i).getMenuImage());


    }


    //---- Contagem de itens no array
    @Override
    public int getItemCount() {
        return nData.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        CardView menuitem;
        TextView menuname;
        TextView menuprice;
        ImageView menuimage;
        CardView buy;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

           menuitem = itemView.findViewById(R.id.menu_item);
           menuname = itemView.findViewById(R.id.menuTitle);
           menuprice =  itemView.findViewById(R.id.menuPrice);
           menuimage =  itemView.findViewById(R.id.menuImage);
           buy = itemView.findViewById(R.id.menuBuy);

        }
    }

    public boolean saveArray() {
        SharedPreferences sp = nContext.getSharedPreferences(SHARED_PREFS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sp.edit();
        Set<String> set = new HashSet<String>();
        set.addAll(list);
        mEdit1.putStringSet("list", set);
        return mEdit1.commit();
    }

    //Metodo para ir buscar o array com as string guardadas

    public ArrayList<String> getArray() {
        SharedPreferences sp = nContext.getSharedPreferences(SHARED_PREFS_NAME, Activity.MODE_PRIVATE);

        //NOTE: if shared preference is null, the method return empty Hashset and not null
        Set<String> set = sp.getStringSet("list", new HashSet<String>());

        return new ArrayList<String>(set);
    }

    //Metodo que permite ao fechar a aplicação guardar as strings

    public void onStop() {
        saveArray();
        //super.onStop();
    }

    public int getRate(){
        return 1;
    }
}

