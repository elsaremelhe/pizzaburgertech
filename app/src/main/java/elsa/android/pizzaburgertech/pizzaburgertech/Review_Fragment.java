package elsa.android.pizzaburgertech.pizzaburgertech;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


public class Review_Fragment extends Fragment {

    ImageView mItem;
    TextView mItemName;
    float mItemPrice;
    TextView mDesc;

    RatingBar mRating;
    EditText mReview;
    CardView mSubmit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mRating = view.findViewById(R.id.ratingBar);
        mReview = view.findViewById(R.id.itemReview);
        mSubmit = view.findViewById(R.id.reviewSubmit);

       /* mRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mReview.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please fill in feedback text box", Toast.LENGTH_LONG).show();
                } else {
                    mReview.setText("");
                    mRating.setRating(0);
                    Toast.makeText(getActivity(), "Thank you for sharing your feedback", Toast.LENGTH_SHORT).show();
                }
            }
        }); */

        return view;
    }
}
