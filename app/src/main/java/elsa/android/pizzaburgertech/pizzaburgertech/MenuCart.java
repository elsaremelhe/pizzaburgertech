package elsa.android.pizzaburgertech.pizzaburgertech;

public class MenuCart {

    private float preco;
    private String nome;
    private int total;
    private int quantidade;

    public MenuCart(float preco, String nome, int quantidade){
        this.preco = preco;
        this.nome = nome;
        this.quantidade = quantidade;
    }


    public float getPreco() {
        return preco;
    }

    public String getNome() {
        return nome;
    }

    public float getTotal() {
        return total;
    }

    public int getQuantidade(){ return quantidade;}



    public void setPreco(int preco) {
        this.preco = preco;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setQuantidade(int quantidade){this.quantidade = quantidade;}
}
