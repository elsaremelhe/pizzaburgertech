package elsa.android.pizzaburgertech.pizzaburgertech;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;

public class CartReyclerAdapter extends RecyclerView.Adapter<CartReyclerAdapter.ViewHolder> {

    private Context nContext;
    private List<MenuCart> nData;



    public CartReyclerAdapter(Context nContext, List<MenuCart> nData) {
        this.nContext = nContext;
        this.nData = nData;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(nContext);
        view = inflater.inflate(R.layout.cart_item, viewGroup, false);
        ViewHolder vHolder = new ViewHolder(view);

        vHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.preco.setText(String.valueOf(nData.get(i).getPreco()));
        viewHolder.cartnome.setText(nData.get(i).getNome());
        viewHolder.quantidade.setText(String.valueOf(nData.get(i).getQuantidade()));
    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView cartnome;
        TextView preco;
        ImageView remove;
        TextView quantidade;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cartnome = itemView.findViewById(R.id.cardTitle);
            preco = itemView.findViewById(R.id.cartPreco);
            remove =itemView.findViewById(R.id.remove);
            quantidade = itemView.findViewById(R.id.quantidade);
        }
    }

}
