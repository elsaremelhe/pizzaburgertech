package elsa.android.pizzaburgertech.pizzaburgertech;

import android.support.v7.widget.CardView;

public class MenuItem {

    private int menuImage;
    private String menuName;
    private float menuPrice;

    public MenuItem(int menuImage, String menuName, float menuPrice) {
        this.menuImage = menuImage;
        this.menuName = menuName;
        this.menuPrice = menuPrice;
    }

    public int getMenuImage() {
        return menuImage;
    }

    public String getMenuName() {
        return menuName;
    }

    public float getMenuPrice() {
        return menuPrice;
    }


    public void setMenuImage(int menuImage) {
        this.menuImage = menuImage;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public void setMenuPrice(float menuPrice) {
        this.menuPrice = menuPrice;
    }
}
