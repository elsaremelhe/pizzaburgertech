package elsa.android.pizzaburgertech.pizzaburgertech;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText mUser;
    EditText mPassword;
    CardView mLogin;
    TextView mRegister;
    DatabaseHelper db;
    String utilizador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        db = new DatabaseHelper(this);

        mUser = findViewById(R.id.loginUser);
        mPassword = findViewById(R.id.loginPassword);
        mLogin = findViewById(R.id.loginButton);
        mRegister = findViewById(R.id.registerText);

        utilizador = mUser.getText().toString();

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = mUser.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                Boolean res = db.checkUser(user, password);

                if (res) {
                    Toast.makeText(MainActivity.this, "Logged in", Toast.LENGTH_SHORT).show();
                    Intent logged = new Intent(MainActivity.this, LoggedAppActivity.class);
                    logged.putExtra("user", utilizador);
                    startActivity(logged);
                } else {
                    Toast.makeText(MainActivity.this, "Loggin Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
