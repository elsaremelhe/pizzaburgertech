package elsa.android.pizzaburgertech.pizzaburgertech;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.MenuItemHoverListener;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Menu_Fragment extends Fragment {

    List<MenuItem> menuList = new ArrayList<>();
    Dialog dialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        dialog = new Dialog(this.getContext());


        menuList.add(new MenuItem(R.drawable.ham1, "MacBurger", 4.59f));
        menuList.add(new MenuItem(R.drawable.ham2, "MacBurger2", 3.99f));
        menuList.add(new MenuItem(R.drawable.ham3, "MacBurger3", 7.59f));
        menuList.add(new MenuItem(R.drawable.pizza1, "HutPizza", 4.59f));
        menuList.add(new MenuItem(R.drawable.pizza2, "HutPizza2", 5.99f));
        menuList.add(new MenuItem(R.drawable.pizza3, "HutPizza3", 6.59f));


        //---- Adição da lista ao RecyclerView usando um Adapter, ordenados em lista

        RecyclerView nRecView = view.findViewById(R.id.menuItem);
        MenuRecyclerAdapter nAdapt = new MenuRecyclerAdapter(this.getContext(), menuList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        nRecView.setLayoutManager(layoutManager);
        nRecView.setAdapter(nAdapt);

        return view;

    }

}
