package elsa.android.pizzaburgertech.pizzaburgertech;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.time.Instant;

public class Profile_Fragment extends Fragment {

    TextView mUsername;
    CardView mLogOut;
    DatabaseHelper db;
    String name;
    TextView points;
    Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //db = new DatabaseHelper(getActivity());
        //Cursor c = db.getAllEntries();
        //c.moveToFirst();

        mUsername = view.findViewById(R.id.profileName);
        points = view.findViewById(R.id.profilePoints);

        points.setText(String.valueOf(EncomendasFragment.pontos));

        if(getArguments() != null) {
            String getuser = getArguments().getString("user");
            mUsername.setText(getuser);
        } /*else{
            Toast.makeText(getActivity(), "WRONG", Toast.LENGTH_SHORT).show();
        }*/

        getNameOnScreen();
        //mUsername.setText(c.getString(1));

        //TextView profileName = view.findViewById(R.id.profileName);
        //String userName = db.getUserName("name");
        //String user = db.getUserName(userName);

        //profileName.setText(String.valueOf(user));

        mLogOut = view.findViewById(R.id.buttonLogOut);

        mLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return view;
    }

    public void getNameOnScreen(){
        db= new DatabaseHelper(getActivity());
        Cursor c = db.getAllEntries();
        c.moveToFirst();

        mUsername.setText(c.getString(1));
    }


}
