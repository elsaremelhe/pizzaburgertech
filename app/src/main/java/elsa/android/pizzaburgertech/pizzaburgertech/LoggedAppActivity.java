package elsa.android.pizzaburgertech.pizzaburgertech;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class LoggedAppActivity extends AppCompatActivity {

    String utilizador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_app);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame, new Pagina_Inicial_Fragment()).commit();
        setTitle("News and Promotions");


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {

                        case R.id.nav_promo:
                            selectedFragment = new Pagina_Inicial_Fragment();
                            setTitle("News and Promotions");
                            break;

                        case R.id.nav_profile:
                            selectedFragment = new Profile_Fragment();
                            setTitle("User Profile");

                            Bundle bundle = new Bundle();
                            bundle.putString("user", utilizador);
                            Profile_Fragment obj = new Profile_Fragment();
                            obj.setArguments(bundle);

                            break;

                        case R.id.nav_cart:
                            selectedFragment = new EncomendasFragment();
                            setTitle("Shopping cart");
                            break;

                        case R.id.nav_menu:
                            selectedFragment = new Menu_Fragment();
                            setTitle("Menus");
                            break;

                        case R.id.nav_help:

                            setTitle("Help");
                            break;


                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, selectedFragment).commit();

                    return true;
                }
            };

}
