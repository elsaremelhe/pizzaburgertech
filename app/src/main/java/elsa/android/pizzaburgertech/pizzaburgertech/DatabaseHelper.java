package elsa.android.pizzaburgertech.pizzaburgertech;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="register.db";
    public static final String TABLE_NAME="registeruser";
    public static final String COL_1="ID";
    public static final String COL_2="name";
    public static final String COL_3="address";
    public static final String COL_4="postalcode";
    public static final String COL_5="phonenum";
    public static final String COL_6="email";
    public static final String COL_7="password";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE registeruser(ID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, address TEXT, postalcode TEXT, phonenum TEXT, email TEXT, password TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long addUser(String name, String address, String postalcode, String phonenum, String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("address", address);
        contentValues.put("postalcode", postalcode);
        contentValues.put("phonenum", phonenum);
        contentValues.put("email", email);
        contentValues.put("password", password);
        long res = db.insert("registeruser", null, contentValues);
        db.close();
        return res;
    }

    public boolean checkUser(String email, String password){
        String[] columns = { COL_1 };
        SQLiteDatabase db = getReadableDatabase();
        String selection = COL_6 + "=?" + " and " + COL_7 + "=?";
        String[] selectionArgs = {email, password};
        Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        if(count > 0){
            return true;
        }

        if(count <= 0){
            return false;
        }
        return false;

    }

    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.close();
    }

    public Cursor getAllEntries(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        int count = cursor.getCount();

        if(count>0)
            return cursor;
        else
            return null;
    }

    public String getUserName(String userName){
        SQLiteDatabase db = this.getReadableDatabase();
        //String selection = COL_2;
        //String id_User= COL_1;
        //String name = COL_2;
        //Cursor cursor = db.query(TABLE_NAME,new String[]{userName}, selection, null, null, null, null);

        String id_User = COL_1;

        Cursor cursor1;
        cursor1 = db.rawQuery("select name from registeruser where ID = ?", new String[]{id_User});


        userName = cursor1.getString(cursor1.getColumnIndex("name"));
        cursor1.close();
        return userName;
    }
}
