package elsa.android.pizzaburgertech.pizzaburgertech;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class EncomendasFragment extends Fragment {


    List<MenuCart> menuCart = MenuRecyclerAdapter.menuCart;
    Dialog dialog;
    static int pontos = 0;

    public void adicionaPontos(){
        pontos = pontos + 5;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_encomendas, container, false);

        dialog = new Dialog(this.getContext());
        dialog.setContentView(R.layout.fragment_confirm_encomenda);

        Button confirm = view.findViewById(R.id.confirm);


        //menuCart.add(new MenuCart(4.59f, "MacBurger", 1));
        //menuCart.add(new MenuCart(5.99f, "HutPizza2", 1));

        //---- Adição da lista ao RecyclerView usando um Adapter, ordenados em lista

        RecyclerView nRecView = view.findViewById(R.id.encomendasCart);
        CartReyclerAdapter cartAdapt = new CartReyclerAdapter(this.getContext(), menuCart);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        nRecView.setLayoutManager(layoutManager);
        nRecView.setAdapter(cartAdapt);

        TextView total = view.findViewById(R.id.cardTotal);

        float totalCart = 0;

            for (MenuCart p : menuCart) {
                totalCart += p.getPreco();
            }


        total.setText(String.valueOf(totalCart));



        confirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                Button cancelar = dialog.findViewById(R.id.cancel_ship);
                Button confirmar = dialog.findViewById(R.id.confirmar_ship);

                dialog.show();

                System.out.println("Tamanho do array: " + menuCart.size());

                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                confirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            menuCart.clear();
                            adicionaPontos();
                            System.out.println("Pontos: " + pontos);
                            System.out.println("Tamanho do array: " + menuCart.size());
                            cartAdapt.notifyDataSetChanged();
                            dialog.dismiss();

                    }
                });



            }
        });
        return view;
    }

}
