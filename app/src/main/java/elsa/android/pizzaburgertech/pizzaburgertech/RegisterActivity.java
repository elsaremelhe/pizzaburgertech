package elsa.android.pizzaburgertech.pizzaburgertech;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.library.NavigationBar;
import com.library.NvTab;

public class RegisterActivity extends AppCompatActivity {

    EditText mName;
    EditText mAddress;
    EditText mPostalCode;
    EditText mPhone;
    EditText mEmail;
    EditText mPassword;
    EditText mConfPassword;
    CardView submit;
    CardView back;
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        mName = findViewById(R.id.nameUser);
        mAddress = findViewById(R.id.addressUser);
        mPostalCode = findViewById(R.id.postalCodeUser);
        mPhone = findViewById(R.id.phoneUser);
        mEmail = findViewById(R.id.emailUser);
        mPassword = findViewById(R.id.passwordUser);
        mConfPassword = findViewById(R.id.confPasswordUser);

        submit = findViewById(R.id.submitRegister);
        back = findViewById(R.id.backRegister);

        db = new DatabaseHelper(this);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mName.getText().toString().trim();
                String address = mAddress.getText().toString().trim();
                String postalcode = mPostalCode.getText().toString().trim();
                String phone = mPhone.getText().toString().trim();
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                String confpassword = mConfPassword.getText().toString().trim();


                //--- condições de registo

                if (name.isEmpty() || address.isEmpty() || postalcode.isEmpty() || phone.isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "All information must be filled out", Toast.LENGTH_SHORT).show();
                } else {
                    if (email.isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();
                    } else {
                        if (password.isEmpty()) {
                            Toast.makeText(RegisterActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                        } else {
                            if (password.equals(confpassword)) {
                                long val = db.addUser(name, address, postalcode, phone, email, password);
                                if (val > 0) {
                                    Toast.makeText(RegisterActivity.this, "Account Created", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Registration Error", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(RegisterActivity.this, "Passwords don't match", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

